(function () {
    angular
        .module("weddingGramApp")
        .config(weddingGramAppConfig)
    weddingGramAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function weddingGramAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('init',{
                url : '/',
                templateUrl :'../app/home/home.html',
                controller : 'PostListCtrl',
                controllerAs : 'ctrl'
            })
            .state('home',{
                url : '/home',
                templateUrl :'./home/home.html',
                controller : 'PostListCtrl',
                controllerAs : 'ctrl'
            })
            .state('profile',{
                url : '/profile',
                templateUrl :'../app/profile/profile.html',
                controller : 'PostListCtrl',
                controllerAs : 'ctrl'
            })
            .state('back',{
                url : '/back',
                templateUrl :'../app/home/home.html',
                controller : 'PostListCtrl',
                controllerAs : 'ctrl'
            })
            .state('comments',{
                url : '/comments/:data',
                templateUrl :'../app/comment/comment.html',
                controller : 'CommentDetailCtrl',
                controllerAs : 'ctrl'
            })

        $urlRouterProvider.otherwise("/");


    }
})();