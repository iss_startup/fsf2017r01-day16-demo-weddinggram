(function () {
    angular
        .module("weddingGramApp", [
            "ngFileUpload",
            "ui.router"
        ]);

})();