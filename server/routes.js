'use strict';

var UserController = require("./api/user/user.controller");
var PostController = require("./api/post/post.controller");
var CommentController = require("./api/comment/comment.controller");
var AWSController = require("./api/aws/aws.controller");
var express = require("express");
var config = require("./config")

module.exports = function (app) {
    app.get('/api/aws/createS3Policy', PostController.list);

    app.get('/api/posts', PostController.list);
    app.get('/api/posts/image/:url', PostController.showImage);

    app.get('/api/posts/me', PostController.me);
    app.post('/api/posts', PostController.create);
    app.get('/api/posts/:id', PostController.get);
    app.post('/api/posts/:id', PostController.update);
    app.delete('/api/posts/:id', PostController.remove);
    app.post('/api/posts/:id/like', PostController.likePost);

    app.get('/api/users', UserController.list);
    app.post('/api/users', UserController.create);
    app.get('/api/users/:id', UserController.get);
    app.get('/api/users/:id/posts', PostController.listByUser);
    app.post('/api/users/:id', UserController.update);
    app.delete('/api/users/:id', UserController.remove);

    app.post('/api/comments', CommentController.create);
    app.delete('/api/comments/:id', CommentController.remove);
    app.get('/api/posts/:postId/comments', CommentController.byPosts);

    app.post('/api/aws/s3-policy', AWSController.getSignedPolicy);

    app.use(express.static(__dirname + "/../client/"));

};


